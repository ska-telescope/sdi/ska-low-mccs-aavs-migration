# ska-low-mccs-aavs-migration


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


# ska-low-mccs-aavs-migration

[![Documentation Status](https://readthedocs.org/projects/ska-low-mccs-aavs-migration/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-low-mccs-aavs-migration/en/latest/?badge=latest)


This project contains metadata utility scripts to generate a Metadata file from observations that are according to [ADR-55](https://confluence.skatelescope.org/display/SWSI/ADR-55+Definition+of+metadata+for+data+management+at+AA0.5). 



## Installation

This project is structure to use python scripts and ansible playbooks, both for different porpuses but both related with aavs data migration. For any of this two operation, there is a need to install some dependencies in your system. 

For this in this project we make use of the standardized tool in SKAO **poetry**. If poetry is not installed in the machine where you intend to use this repository, please run the following to install it:

```
sudo apt update
sudo apt install -y curl git build-essential libboost-python-dev libtango-dev
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python3 get-pip.py && rm get-pip.py
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
```

## How to Use

Clone this repo: 
```
git clone https://gitlab.com/ska-telescope/sdi/ska-low-mccs-aavs-migration.git
cd ska-low-mccs-aavs-migration
```

Install python requirements for operating it:
```
$ poetry install
```

Go inside the environment created:
```
$ poetry shell
```

### Generate Metadata 

This project also make use of make targets to group commands and make it easier for the users to run specific tasks, such as the one for generating Metadata.

Before jumping into what this target is and how to use it, first lets go through how the generation of metadata will work. At the moment in this repo **3** different file types are supported to generate metadata from and those are:

* **DAT**
* **DADA**
* **HDF5**

The extraction of metadata for all of the above is different and how the metadata values are extracted can be seen in the page [How the Metadata is generated](https://confluence.skatelescope.org/display/SE/How+the+Metadata+is+generated). So in this section we will not go into much detail, on which fields are generated and how. 

To run the generation of metadata scripts all you need to do is simply:
```
$ make generate-metadata
```

This command will prompt you with some information that it needs, such as :

1. Choose on of the available file types [dat,dada,hdf5] : For this you need to choose which type of the supported file types the metadata will be generated from

2. Enter the full path of where the data is (i.e /data/2022_12_13_jnt_meteor/): For this you will need to specify the full path of where the observation folder is located, so that the script can iterate over this folder to find the files that it needs. **It is important that the path ends with /**

3. Enter a 5-digit number for the execution block identifier. Run 'mc ls acacia/aavs2/product': Each product should have an unique number of 5 digits like (00001). To check the ones that were already picked, please run the command ```mc ls acacia/aavs2/product```.

4. Enter the Path where the metadata will be saved (i.e /home/bruno): For this you need to give as an input the path where the metadata will be saved. This needs to be a path where your user has write permissions, so it can write the file to it. This was needed because some users do not have write permissions to the observation folders. **It is important that the path ends without /**

5. After all of this inputs, the metadata will be generated at /home/bruno/data/2022_12_13_jnt_meteor/ path according to that example.


### Migrate data and Metadata


#### Requirements

For this Migration to work firstly you will need to configure your environemnt with rclone configuration, and that configuration should be pointing to acacia. The configuration should leave under ```~/.config/rclone/rclone.conf```, and it should contain something like:


```
[pawsey0884]
type = s3
provider = Ceph
endpoint = https://ingest.pawsey.org.au
access_key_id = <secret_key_id>
secret_access_key = <secret_access_key>
```

#### Migration

After the metadata has been generated and lets take the example above, assuming that is was generated at  ```/home/bruno/data/2022_12_13_jnt_meteor/obj_1/ska-data-product.yaml ``` and ```/home/bruno/data/2022_12_13_jnt_meteor/obj_2/ska-data-product.yaml ```

This a 2 object case where the metadata was generated for both of them. As in the [Generate Metadata](#generate-metadata) there is a make target what will help with this procedure:

```
$ make migrate-acacia
```

This command will prompt you with some information that it needs, such as :

1. Enter Metadata full path (i.e /home/aavsro/data_archive/2021_07_09_pulsars/) : For this you need to specify the observation that you want to migrate in the example that is in the beggining of this section this should be ```/home/bruno/data/2022_12_13_jnt_meteor/```

2. Enter Metadata home path (i.e /home/aavsro): This should be the home path that is not related to the observation itself, this should be the path that was given as input in step **4** of the generate metadata target. In the example that we are using that should be ```/home/bruno```


And Shebang (#!) the data should start to migrate to the acacia S3 Storage.

## Support

If you have any questions about how to use this repository, or any bug that you have found. Please create a ticket in https://jira.skatelescope.org/projects/STS/. 


