#! /bin/bash

# cp bash scripts to local bin folder
mkdir -p $HOME/.local/bin
cp src/scripts/* $HOME/.local/bin/.

# cp python executables to local bin folder
cp -r src/ska_low_mccs_metadata_utility/* $HOME/.local/bin/.

# cp python packages to local lib folder
mkdir -p $HOME/.local/lib/python3/site-packages
cp -r src/ska_low_mccs_metadata_utility $HOME/.local/lib/python3/site-packages/.

# install yq in the local bin folder
wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O $HOME/.local/bin/yq && chmod +x $HOME/.local/bin/yq

echo 'export PATH=$PATH:$HOME/.local/bin' >> $HOME/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python3/site-packages' >> $HOME/.bashrc

