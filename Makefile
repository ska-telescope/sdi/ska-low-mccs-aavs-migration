
# include core make support
include .make/base.mk

# Include Python support
include .make/python.mk

# include Oci images support
-include .make/oci.mk

# include ansible support
include .make/ansible.mk

# include release support
-include .make/release.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak


BASE_PATH?=$(shell cd "$(dirname "$1")"; pwd -P)

FILE_TYPES_SUPPORTED = ["dat", "dada", "hdf5"]

# Path to ansible playbooks
ANSIBLE_COLLECTIONS=playbooks/

.PHONY: generate-metadata migrate-acacia

migrate-acacia:
    @read -p "Enter Metadata full path (i.e /home/aavsro/data_archive/2021_07_09_pulsars/) : " METADATA_FULL_PATH; \
    read -p "Enter Metadata home path (i.e /home/aavsro): " METADATA_HOME_PATH; \
    ansible-playbook -e file_metadata_path="$$METADATA_FULL_PATH" -e metadata_path="$$METADATA_HOME_PATH" playbooks/clone_files.yaml; \

generate-metadata:
	@read -p "Choose on of the available file types ${FILE_TYPES_SUPPORTED}: " arg1; \
	until echo $(FILE_TYPES_SUPPORTED) | grep -qw "$$arg1"; do \
        echo "Invalid choice. Choose one of the available file types: $(FILE_TYPES_SUPPORTED)"; \
        read -p "Choose one of the available file types (${FILE_TYPES_SUPPORTED}): " arg1; \
    done; \
	read -p "Enter the full path of where the data is (i.e /data/2022_12_13_jnt_meteor/) : " arg2; \
	read -p "Enter a 5-digit number for the execution block identifier. Run 'mc ls acacia/aavs2/product' : " arg3; \
    while [[ ! $$arg3 =~ ^[0-9]{5}$$ ]]; do \
        read -p "Invalid input. Enter a 5-digit number for argument 3: " arg3; \
    done; \
	read -p "Enter the Path where the metadata will be saved (i.e /home/bruno) " arg4; \
	python3 src/ska_low_mccs_metadata_utility/$${arg1}_metadata_utility.py $$arg2 $$arg3 $$arg4
