FROM python:3.10-slim

# Install required packages
RUN apt-get update && apt-get install -y \
    nano \
    vim \
    curl \
    unzip \
    wget

# Install Minio client
RUN curl -O https://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x mc && \
    mv mc /usr/bin/

# Install rclone
RUN curl https://rclone.org/install.sh | bash

# Install yq
RUN wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && \
    chmod +x /usr/bin/yq

# intall poetry
RUN pip install poetry "poetry==1.4.2"

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

RUN mkdir -p /app/src
WORKDIR /app

# Copy poetry files and install dependencies
COPY pyproject.toml poetry.lock /app/
RUN poetry install --without dev --no-root && rm -rf $POETRY_CACHE_DIR

# Copy code
COPY ./src /app/src

# Install again to add the code
RUN poetry install --without dev

CMD ["/bin/sh"]
