#! /usr/bin/python3
"""
    Module used to obtain Metadata information from
    hdf5 file types. Metadata information obtained
    is the one according to ADR-55
"""

import argparse
import glob
import gzip
import os
import random
import re
from io import BytesIO

import h5py
import yaml

from ska_low_mccs_metadata_utility.metadata_utility_class import MetadataClass


class HDF5Metadata(MetadataClass):
    """
    Utility class to obtain Metadata information from
    hdf5 file types. Metadata information obtained
    is the one according to ADR-55
    """

    # none HDF5 files to be migrated
    extensions_for_secondary = ["*.fits", "*.uvfits", "*.restor", "*.txt"]

    def process_targets(self):
        """
        Main Method to process observation folder.
        """

        hdf5_files = []
        for ext in ("*.hdf5", "*.hdf5.gz"):
            hdf5_files.extend(
                glob.glob(
                    os.path.join(self.folder_path + "**/*", ext),
                    recursive=True,
                )
            )

        min_time = None
        max_time = None
        em_min = None
        em_max = None
        start_frequency_channel = None

        # Iterate over each HDF5 file

        first = True
        percentage_meter = 1

        for hdf5_file in hdf5_files:
            print(
                "Percentage: "
                + str(int((percentage_meter / len(hdf5_files)) * 100))
            )

            percentage_meter = percentage_meter + 1

            file_name = os.path.basename(hdf5_file)
            description = "null"

            # Get the last component of the path
            basename = os.path.basename(self.folder_path.rstrip("/"))

            # Extract the year, month, and day parts
            _split = basename.split("_")

            # Put them together in the desired format
            self.observation_date_time = _split[0] + _split[1] + _split[2]

            # If HDF5 is gzipped, we need to unzip it first
            if "gz" in hdf5_file:
                # Decompress the .gz file into a BytesIO buffer
                with gzip.open(hdf5_file, "rb") as f_in:
                    hdf5_file = BytesIO(f_in.read())

            # Read hdf5 file
            f = h5py.File(hdf5_file)

            # Get description from observation if it
            if "observation_info" in f:
                if "description" in f["observation_info"].attrs:
                    description = f["observation_info"].attrs["description"]

            self.add_file_to_metadata(description, file_name)
            self.metadata[
                "files"
            ].append(  # pylint: disable=access-member-before-definition
                self.add_file_to_metadata(description, file_name)
            )

            if first is True:
                # Do it for only first iteration

                out_file_path = self.search_file(self.folder_path, "out")
                if out_file_path:
                    self.metadata["files"].append(
                        self.add_file_to_metadata("out file", "out")
                    )
                    self.metadata = self.read_out_file(
                        out_file_path, self.metadata
                    )
                else:
                    self.metadata["obscore"]["target_name"] = "All Sky"
                    if self.search_file(self.folder_path, "hdf5.hdr"):
                        self.metadata["obscore"]["s_ra"] = "null"
                        self.metadata["obscore"]["s_dec"] = "null"
                    else:
                        if self.search_file(self.folder_path, "header.txt"):
                            self.metadata["obscore"]["s_ra"] = "null"
                            self.metadata["obscore"]["s_dec"] = "null"
                        else:
                            self.metadata["obscore"]["s_ra"] = "All Sky"
                            self.metadata["obscore"]["s_dec"] = "All Sky"

                # Tsamp comes in milisecons, and we want it to be seconds
                self.metadata["obscore"]["t_resolution"] = float(
                    f["root"].attrs["tsamp"] / 1000
                )

                self.metadata["obscore"]["pol_xel"] = float(
                    f["root"].attrs["n_stokes"]
                )

                if self.metadata["obscore"]["pol_xel"] == 4:
                    self.metadata["obscore"]["pol_states"] = "/XX/XY/YX/YY/"
                else:
                    self.metadata["obscore"]["pol_states"] = "null"

                pb_identifier = random.randint(10000, 99999)
                while pb_identifier in self.used_numbers:
                    pb_identifier = random.randint(10000, 99999)

                self.used_numbers.append(pb_identifier)

                self.add_pb_block(pb_identifier)

            else:
                # Do it for only after first  iteration
                if (
                    float(f["root"].attrs["tsamp"] / 1000)
                    != self.metadata["obscore"]["t_resolution"]
                ):
                    print(
                        "❌ Descrepency in TSAMP: - Previous: "
                        + str(self.metadata["obscore"]["t_resolution"])
                        + "  New: "
                        + str((f["root"].attrs["tsamp"] / 1000))
                    )
                    # Tsamp comes in milisecons, and we want it to be seconds
                    self.metadata["obscore"]["t_resolution"] = float(
                        f["root"].attrs["tsamp"] / 1000
                    )

                if (
                    float(f["root"].attrs["n_stokes"])
                    != self.metadata["obscore"]["pol_xel"]
                ):
                    print("❌ Descrepency in: n_stokes")

            # Do it for every file

            if "observation_info" in f:
                if "station_config" in f["observation_info"].attrs:
                    lines = (
                        f["observation_info"]
                        .attrs["station_config"]
                        .split("\n")
                    )
                    for line in lines:
                        # Ignore the lines starting with '#' (comments)
                        if line.strip().startswith("#"):
                            continue
                        # Extract start_frequency_channel
                        if "start_frequency_channel" in line:
                            match = re.search(
                                r"start_frequency_channel:\s*([\d.]+[eE]?[+-]?\d*)",  # Noqa E501
                                line,
                            )
                            if match:
                                start_frequency_channel = float(match.group(1))

                            break
                    bandwidth = re.search(
                        r"bandwidth:\s*(\S+)",
                        f["observation_info"].attrs["station_config"],
                    ).group(1)

            else:
                channel_id = f["root"].attrs["channel_id"]
                bandwidth = 6.25e6

                if channel_id != 0:
                    start_frequency_channel = channel_id * (400 / 512) * 1e6

                else:
                    # IF no channel or it equals 0
                    # try to fetch channel from folder name
                    folder_name = os.path.basename(
                        self.folder_path.rstrip("/")
                    )
                    match_channel = re.search(r"ch(\d+)", folder_name)

                    if match_channel:
                        # If a match is found, extract the channel number
                        start_frequency_channel = (
                            int(match_channel.group(1)) * (400 / 512) * 1e6
                        )

            if start_frequency_channel:
                if not em_min or start_frequency_channel < em_min:
                    em_min = float(start_frequency_channel)

                if not em_max or start_frequency_channel > em_max:
                    em_max = float(start_frequency_channel)

            if not min_time or f["root"].attrs["ts_start"] < min_time:
                min_time = f["root"].attrs["ts_start"]

            if not max_time or f["root"].attrs["ts_end"] > max_time:
                max_time = f["root"].attrs["ts_end"]

            first = False

        if min_time:
            #  Converting times to MJD with 14 decimal cases
            self.metadata["obscore"]["t_min"] = float(
                round((min_time / 86400) + 40587, 14)
            )
        else:
            self.metadata["obscore"]["t_min"] = "null"

        if max_time:
            self.metadata["obscore"]["t_max"] = float(
                round((max_time / 86400) + 40587, 14)
            )
        else:
            self.metadata["obscore"]["t_max"] = "null"

        if min_time and max_time:
            self.metadata["obscore"]["t_exptime"] = float(
                round(
                    self.metadata["obscore"]["t_max"]
                    - self.metadata["obscore"]["t_min"],
                    14,
                )
            )
        else:
            self.metadata["obscore"]["t_exptime"] = "null"

        speed_of_light = 299792458  # m/s

        if em_min:
            self.metadata["obscore"]["em_min"] = float(speed_of_light / em_min)
        else:
            self.metadata["obscore"]["em_min"] = "null"

        if em_min:
            self.metadata["obscore"]["em_max"] = float(speed_of_light / em_min)
            self.metadata["obscore"]["em_res_power"] = float(
                float(bandwidth) / em_min
            )
        else:
            self.metadata["obscore"]["em_max"] = "null"
            self.metadata["obscore"]["em_res_power"] = "null"

        self.metadata["files"].append(
            self.add_file_to_metadata(
                "Metadata File Description according with ADR-55",
                self.metadata_file_name,
            )
        )

        secondary_glob_paths = [
            self.folder_path + "",
            self.folder_path + "*/",
        ]

        secondary_files_list = self.get_secondary_files(
            secondary_glob_paths, self.extensions_for_secondary
        )

        for secondary_file in secondary_files_list:
            # Add file to context
            secondary_file_basename = str(secondary_file).replace(
                self.folder_path, ""
            )
            self.metadata["files"].append(
                self.add_file_to_metadata("", secondary_file_basename)
            )

        self.add_execution_block()

        metadata_folder_path = (
            self.metadata_path + self.folder_path + "ska-data-product.yaml"
        )

        os.makedirs(os.path.dirname(metadata_folder_path), exist_ok=True)

        # Writing the dictionary to the YAML file
        with open(metadata_folder_path, "w", encoding="UTF-8") as file:
            yaml.dump(self.metadata, file)


if __name__ == "__main__":
    # Create an argument parser
    parser = argparse.ArgumentParser(
        description="Process HDF5 files in a directory."
    )

    # Add an argument for the directory path
    parser.add_argument(
        "directory_path", help="Path to the directory containing HDF5 files"
    )

    # Add an argument for the eb_identifier
    parser.add_argument("eb_identifier", help="EB Identifier")

    # Add an argument for the eb_identifier
    parser.add_argument(
        "metadata_path", help="Path where metadata will be stored"
    )

    # Parse the command-line arguments
    args = parser.parse_args()

    # Access the directory path and eb_identifier from the arguments
    directory_path = args.directory_path
    eb_identifier = args.eb_identifier
    metadata_path = args.metadata_path

    # Now, you can use the 'directory_path'
    # and 'eb_identifier' variables in your code
    test = HDF5Metadata(directory_path, eb_identifier, metadata_path)

    test.process_targets()
