"""
    Module Base Utility to obtain Metadata information from
    different file types. Metadata information obtained
    is the one according to ADR-55
    """

import glob
import os


class MetadataClass:
    """
    Base Utility class to obtain Metadata information from
    different file types. Metadata information obtained
    is the one according to ADR-55
    """

    # Set to store the generated numbers
    used_numbers = []

    observation_date_time = ""
    metadata_file_name = "ska-data-product.yaml"

    def __init__(self, folder_path, eb_identifier, metadata_path):
        self.metadata = {
            "execution_block": "",
            "config": {
                "processing_block": "",
                "processing_script": "receive",
                "sub_system": "ska-low-mccs",
            },
            "files": [],
            "obscore": {
                "dataproduct_type": "",
                "calib_level": 1,
                "target_name": "",
                "s_ra": "",
                "s_dec": "",
                "t_min": "",
                "t_max": "",
                "t_exptime": "",
                "t_resolution": "",
                "o_ucd": "null",
                "pol_states": "",
                "pol_xel": "",
                "instrument_name": "aavs2",
                "facility_name": "ska-low",
                "em_min": "",
                "em_max": "",
                "em_res_power": "",
            },
        }
        self.folder_path = folder_path
        self.eb_identifier = eb_identifier
        self.metadata_path = metadata_path

    def add_execution_block(self):
        self.metadata["execution_block"] = "eb-m001-{}-{}".format(
            self.observation_date_time, self.eb_identifier
        )

    def add_pb_block(self, pb_identifier):
        self.metadata["config"]["processing_block"] = "pb-aavs2-{}-{}".format(
            self.observation_date_time, pb_identifier
        )

    def add_file_to_metadata(self, description, file_name):
        new_file = {
            "path": file_name,
            "description": description,
            "status": "done",
        }
        return new_file

    def search_file(self, folder_path, file_name):
        # Find all files with extension in the folder
        file = glob.glob(os.path.join(folder_path, file_name))
        return file

    def get_secondary_files(self, glob_paths, extensions):
        files = []

        for glob_path in glob_paths:
            for ext in extensions:
                files.extend(glob.glob(os.path.join(glob_path, ext)))

        return files

    def read_out_file(self, out_file_path, target_metadata):
        with open(out_file_path, "r", encoding="UTF-8") as out_file:
            lines = out_file.readlines()
        for line in lines:
            if "point_station_radec.sh" in line:
                # Split the line by spaces
                tokens = line.split()
                if tokens:
                    target_metadata["obscore"]["s_ra"] = float(tokens[1])
                    if len(tokens) > 2:
                        target_metadata["obscore"]["s_dec"] = float(tokens[2])
                        if len(tokens) > 3:
                            target_metadata["obscore"][
                                "instrument_name"
                            ] = tokens[3]
                            if len(tokens) > 4:
                                target_metadata["obscore"][
                                    "target_name"
                                ] = tokens[4]
        return target_metadata
