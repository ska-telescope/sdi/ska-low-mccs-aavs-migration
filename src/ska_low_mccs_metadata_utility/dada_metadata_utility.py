#! /usr/bin/python3
"""
    Module used to obtain Metadata information from
    dada file types. Metadata information obtained
    is the one according to ADR-55
"""

import argparse
import copy
import glob
import gzip
import json
import os
import random

import yaml

from ska_low_mccs_metadata_utility.metadata_utility_class import MetadataClass


class DADAMetadata(MetadataClass):
    """
    Utility class to obtain Metadata information from
    dada file types. Metadata information obtained
    is the one according to ADR-55
    """

    # none dada files to be migrated
    extensions_for_secondary = ["*.fits", "*.ar", "*.txt"]

    def process_targets(self):
        """
        Main Method to process observation folder.
        """
        # Find all files with .hdf5 extension in the folder

        dada_targets, root_files = self.group_dada_files()

        min_time = None
        bw_min = None
        bw_max = None
        fq_min = None
        fq_max = None

        # Get the last component of the path
        basename = os.path.basename(self.folder_path.rstrip("/"))

        # Extract the year, month, and day parts
        _split = basename.split("_")

        # Put them together in the desired format
        self.observation_date_time = _split[0] + _split[1] + _split[2]

        for targets in dada_targets:
            pb_identifier = ""
            first = True
            target_metadata = copy.deepcopy(self.metadata)

            if targets in root_files:
                channel_path = self.folder_path + targets + "/"
                metadata_path_to_file = ""

                pointing_out = self.search_file(channel_path, "pointing.out")[
                    0
                ]

                interval = 900

                if pointing_out:
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "Pointing.out file",
                            metadata_path_to_file + "pointing.out",
                        )
                    )
                    target_metadata = self.read_out_file(
                        pointing_out, target_metadata
                    )

                for dada_file in root_files[targets]:
                    if first is True:
                        # Add file to context

                        file_path = channel_path + dada_file

                        # If HDF5 is gzipped, we need to unzip it first
                        if "gz" in dada_file:
                            # Decompress the .gz file into a BytesIO buffer
                            with gzip.open(file_path, "rb") as f_in:
                                dada_file_data = f_in.read()
                        else:
                            with open(file_path, "rb") as file:
                                dada_file_data = file.read()

                        # Convert the binary data to a string and decode it
                        data = dada_file_data[:4096].decode(
                            "utf-8", errors="replace"
                        )

                        # Extract the required lines
                        # and create a JSON-like string
                        lines = data.splitlines()[:-1]  # Remove the last line
                        json_string = (
                            "{"
                            + ",".join(
                                [
                                    '"{}": "{}"'.format(
                                        line.split()[0], line.split()[1]
                                    )
                                    for line in lines
                                ]
                            )
                            + "}"
                        )

                        # Convert JSON string to a Python dictionary
                        json_string = json.loads(json_string)

                        if (
                            not min_time
                            or (
                                float(json_string["UNIXTIME"])
                                + float(json_string["UNIXTIME_MSEC"]) / 1000
                            )
                            < min_time
                        ):
                            min_time = (
                                float(json_string["UNIXTIME"])
                                + float(json_string["UNIXTIME_MSEC"]) / 1000
                            )

                        # Tsamp comes in milisecons,
                        # and we want it to be seconds
                        target_metadata["obscore"]["t_resolution"] = float(
                            float(json_string["TSAMP"]) / 1000
                        )

                        target_metadata["obscore"]["pol_xel"] = float(
                            json_string["NPOL"]
                        )

                        if not bw_min or float(json_string["BW"]) < bw_min:
                            bw_min = float(json_string["BW"])

                        if not bw_max or float(json_string["BW"]) < bw_max:
                            bw_max = float(json_string["BW"])

                        if not fq_min or float(json_string["FREQ"]) < fq_min:
                            fq_min = float(json_string["FREQ"])

                        if not fq_max or float(json_string["FREQ"]) < fq_max:
                            fq_max = float(json_string["FREQ"])

                        pb_identifier = random.randint(10000, 99999)
                        while pb_identifier in self.used_numbers:
                            pb_identifier = random.randint(10000, 99999)

                        self.used_numbers.append(pb_identifier)

                        target_metadata["config"][
                            "processing_block"
                        ] = "pb-aavs2-{}-{}".format(
                            self.observation_date_time, pb_identifier
                        )

                    else:
                        # Do it for only after first  iteration
                        if (
                            float(float(json_string["TSAMP"]) / 1000)
                            != target_metadata["obscore"]["t_resolution"]
                        ):
                            print("Descrepency in: TSAMP")

                        if (
                            float(json_string["NPOL"])
                            != target_metadata["obscore"]["pol_xel"]
                        ):
                            print("Descrepency in: TSAMP")

                    # Add file to context
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "", metadata_path_to_file + dada_file
                        )
                    )

            for channel in dada_targets[targets]:
                channel_path = self.folder_path + targets + "/" + channel + "/"
                metadata_path_to_file = channel + "/"

                pointing_out = self.search_file(channel_path, "pointing.out")[
                    0
                ]

                interval = 900

                if pointing_out:
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "Pointing.out file",
                            metadata_path_to_file + "pointing.out",
                        )
                    )
                    target_metadata = self.read_out_file(
                        pointing_out, target_metadata
                    )

                for dada_file in dada_targets[targets][channel]:
                    if first is True:
                        # Add file to context

                        file_path = channel_path + dada_file

                        # If HDF5 is gzipped, we need to unzip it first
                        if "gz" in dada_file:
                            # Decompress the .gz file into a BytesIO buffer
                            with gzip.open(file_path, "rb") as f_in:
                                dada_file_data = f_in.read()
                        else:
                            with open(file_path, "rb") as file:
                                dada_file_data = file.read()

                        # Convert the binary data to a string and decode it
                        data = dada_file_data[:4096].decode(
                            "utf-8", errors="replace"
                        )

                        # Extract the required
                        # lines and create a JSON-like string
                        lines = data.splitlines()[:-1]  # Remove the last line
                        json_string = (
                            "{"
                            + ",".join(
                                [
                                    '"{}": "{}"'.format(
                                        line.split()[0], line.split()[1]
                                    )
                                    for line in lines
                                ]
                            )
                            + "}"
                        )

                        # Convert JSON string to a Python dictionary
                        json_string = json.loads(json_string)

                        if (
                            not min_time
                            or (
                                float(json_string["UNIXTIME"])
                                + float(json_string["UNIXTIME_MSEC"]) / 1000
                            )
                            < min_time
                        ):
                            min_time = (
                                float(json_string["UNIXTIME"])
                                + float(json_string["UNIXTIME_MSEC"]) / 1000
                            )

                        # Tsamp comes in milisecons,
                        # and we want it to be seconds
                        target_metadata["obscore"]["t_resolution"] = float(
                            float(json_string["TSAMP"]) / 1000
                        )

                        target_metadata["obscore"]["pol_xel"] = float(
                            json_string["NPOL"]
                        )

                        if not bw_min or float(json_string["BW"]) < bw_min:
                            bw_min = float(json_string["BW"])

                        if not bw_max or float(json_string["BW"]) < bw_max:
                            bw_max = float(json_string["BW"])

                        if not fq_min or float(json_string["FREQ"]) < fq_min:
                            fq_min = float(json_string["FREQ"])

                        if not fq_max or float(json_string["FREQ"]) < fq_max:
                            fq_max = float(json_string["FREQ"])

                        pb_identifier = random.randint(10000, 99999)
                        while pb_identifier in self.used_numbers:
                            pb_identifier = random.randint(10000, 99999)

                        self.used_numbers.append(pb_identifier)

                        target_metadata["config"][
                            "processing_block"
                        ] = "pb-aavs2-{}-{}".format(
                            self.observation_date_time, pb_identifier
                        )

                    else:
                        # Do it for only after first  iteration
                        if (
                            float(float(json_string["TSAMP"]) / 1000)
                            != target_metadata["obscore"]["t_resolution"]
                        ):
                            print("Descrepency in: TSAMP")

                        if (
                            float(json_string["NPOL"])
                            != target_metadata["obscore"]["pol_xel"]
                        ):
                            print("Descrepency in: TSAMP")

                    # Add file to context
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "", metadata_path_to_file + dada_file
                        )
                    )

                    first = False

            speed_of_light = 299792458  # m/s
            target_metadata["obscore"]["t_min"] = min_time / 86400 + 40587
            target_metadata["obscore"]["t_max"] = (
                target_metadata["obscore"]["t_min"] + interval
            )
            target_metadata["obscore"]["t_exptime"] = interval

            target_metadata["obscore"]["em_min"] = (
                speed_of_light / fq_min * 1e06
            )
            target_metadata["obscore"]["em_max"] = (
                speed_of_light / fq_max * 1e06
            )
            target_metadata["obscore"]["em_res_power"] = bw_max / fq_max

            # Add file to context
            target_metadata["files"].append(
                self.add_file_to_metadata(
                    "Metadata File Description according with ADR-55",
                    self.metadata_file_name,
                )
            )

            target_metadata["execution_block"] = "eb-m001-{}-{}".format(
                self.observation_date_time, self.eb_identifier
            )
            secondary_glob_paths = [
                channel_path + "",
                channel_path + "*/",
                channel_path + "*/*/",
            ]

            secondary_files_list = self.get_secondary_files(
                secondary_glob_paths, self.extensions_for_secondary
            )

            for secondary_file in secondary_files_list:
                # Add file to context
                secondary_file_basename = str(secondary_file).replace(
                    channel_path, ""
                )
                target_metadata["files"].append(
                    self.add_file_to_metadata("", secondary_file_basename)
                )

            os.makedirs(os.path.dirname(metadata_path), exist_ok=True)

            metadata_folder_path = (
                self.metadata_path
                + self.folder_path
                + targets
                + "/"
                + "ska-data-product.yaml"
            )

            print(metadata_folder_path)

            os.makedirs(os.path.dirname(metadata_folder_path), exist_ok=True)

            # Writing the dictionary to the YAML file
            with open(metadata_folder_path, "w", encoding="UTF-8") as file:
                yaml.dump(target_metadata, file)

    def group_dada_files(self):
        dada_files = []
        dada_targets = {}
        root_path_list = {}

        for ext in ("*.dada", "*.dada.gz"):
            dada_files.extend(
                glob.glob(
                    os.path.join(self.folder_path + "**/", ext), recursive=True
                )
            )

        for file in dada_files:
            file_path = str(file).replace(self.folder_path, "")

            parts = file_path.split("/")

            target_name = parts[0]

            target_path = str(file_path).replace(target_name, "").lstrip("/")

            target_parts = target_path.split("/")

            if target_name not in dada_targets:
                dada_targets[target_name] = {}

            if len(target_parts) == 1:
                if target_name not in root_path_list:
                    root_path_list[target_name] = []
                root_path_list[target_name].append(target_parts[0])
            else:
                dada_targets[target_name].update(
                    self.nested_dict(target_parts, dada_targets[target_name])
                )

        return dada_targets, root_path_list

    def nested_dict(self, lst, dada_targets):
        if len(lst) == 1:
            if isinstance(dada_targets, dict):
                dada_targets = []
            dada_targets.append(lst[0])
            return dada_targets
        else:
            if lst[0] not in dada_targets:
                dada_targets[lst[0]] = {}
            return {lst[0]: self.nested_dict(lst[1:], dada_targets[lst[0]])}


if __name__ == "__main__":
    # Create an argument parser
    parser = argparse.ArgumentParser(
        description="Process HDF5 files in a directory."
    )

    # Add an argument for the directory path
    parser.add_argument(
        "directory_path", help="Path to the directory containing HDF5 files"
    )

    # Add an argument for the eb_identifier
    parser.add_argument("eb_identifier", help="EB Identifier")

    # Add an argument for the eb_identifier
    parser.add_argument(
        "metadata_path", help="Path where metadata will be stored"
    )

    # Parse the command-line arguments
    args = parser.parse_args()

    # Access the directory path
    #  and eb_identifier from the arguments
    directory_path = args.directory_path
    eb_identifier = args.eb_identifier
    metadata_path = args.metadata_path

    # Now, you can use the 'directory_path'
    #  and 'eb_identifier' variables in your code
    test = DADAMetadata(directory_path, eb_identifier, metadata_path)

    test.process_targets()
