#! /usr/bin/python3
"""
    Module used to obtain Metadata information from
    dat file types. Metadata information obtained
    is the one according to ADR-55
"""

import argparse
import copy
import glob
import os
import random
import re

import yaml

from ska_low_mccs_metadata_utility.metadata_utility_class import MetadataClass


class DATMetadata(MetadataClass):
    """
    Utility class to obtain Metadata information from
    dat file types. Metadata information obtained
    is the one according to ADR-55
    """

    extensions_for_secondary = ["*.fits", "*.ar", "*.txt"]

    def process_targets(self):
        """
        Main Method to process observation folder.
        """

        dat_files, root_files = self.group_dat_files()

        min_time = None
        em_min = None
        em_max = None
        interval_value = None
        freq_channel_value = None

        # Get the last component of the path
        basename = os.path.basename(self.folder_path.rstrip("/"))

        # Extract the year, month, and day parts
        _split = basename.split("_")

        # Put them together in the desired format
        self.observation_date_time = _split[0] + _split[1] + _split[2]

        for targets in dat_files:
            pb_identifier = ""
            target_metadata = copy.deepcopy(self.metadata)

            if targets in root_files:
                channel_path = self.folder_path + targets + "/"
                metadata_path_to_file = ""

                pointing_out = self.search_file(channel_path, "pointing.out")

                if pointing_out:
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "Pointing.out file",
                            metadata_path_to_file + "pointing.out",
                        )
                    )
                    target_metadata = self.read_out_file(
                        pointing_out[0], target_metadata
                    )

                out_file = self.search_file(channel_path, "out")

                if out_file:
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "out file", metadata_path_to_file + "out"
                        )
                    )
                    interval_pattern = r"interval\s+=\s+(\d+)"
                    freq_channel_pattern = r"freq_channel\s+=\s+(\d+)"
                    with open(out_file[0], "r", encoding="UTF-8") as file:
                        lines = file.readlines()

                    for index, line in enumerate(lines):
                        if "PARAMETERS:" in line:
                            for sub_line in lines[index + 2 :]:  # noqa E203
                                if (
                                    "###################################################"  # noqa E501
                                    in sub_line
                                ):
                                    break
                                interval_match = re.search(
                                    interval_pattern, sub_line
                                )
                                freq_channel_match = re.search(
                                    freq_channel_pattern, sub_line
                                )
                                if interval_match:
                                    interval_value = int(
                                        interval_match.group(1)
                                    )
                                if freq_channel_match:
                                    freq_channel_value = int(
                                        freq_channel_match.group(1)
                                    )

                    if freq_channel_value:
                        start_frequency_channel = (
                            freq_channel_value * (400 / 512) * 1e6
                        )

                else:
                    # IF no channel or it equals 0 try to
                    # fetch channel from folder name
                    folder_name = os.path.basename(
                        self.folder_path.rstrip("/")
                    )
                    match_channel = re.search(r"ch(\d+)", folder_name)

                if start_frequency_channel:
                    if not em_min or start_frequency_channel < em_min:
                        em_min = float(start_frequency_channel)

                    if not em_max or start_frequency_channel > em_max:
                        em_max = float(start_frequency_channel)

                for dat_file in root_files[targets]:
                    # example var :
                    # channel_4_1604456020.385211.dat
                    # extract 1604456020.385211 timestamp

                    parts = dat_file.split("_")
                    if len(parts) == 3:
                        UNIXTIME = parts[2].split(".")[
                            0
                        ]  # Extract the part before the dot
                        UNIXTIME_MSEC = parts[2].split(".")[1]
                        timestamp = (
                            float(UNIXTIME) + float(UNIXTIME_MSEC) / 1000
                        )
                        if not min_time or timestamp < min_time:
                            min_time = timestamp

                    # Add file to context
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "", metadata_path_to_file + dat_file
                        )
                    )

            for channel in dat_files[targets]:
                channel_path = self.folder_path + targets + "/" + channel + "/"
                metadata_path_to_file = channel + "/"

                pointing_out = self.search_file(channel_path, "pointing.out")

                if pointing_out:
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "Pointing.out file",
                            metadata_path_to_file + "pointing.out",
                        )
                    )
                    target_metadata = self.read_out_file(
                        pointing_out[0], target_metadata
                    )

                out_file = self.search_file(channel_path, "out")

                if out_file:
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "out file", metadata_path_to_file + "out"
                        )
                    )
                    interval_pattern = r"interval\s+=\s+(\d+)"
                    freq_channel_pattern = r"freq_channel\s+=\s+(\d+)"
                    with open(out_file[0], "r", encoding="UTF-8") as file:
                        lines = file.readlines()

                    for index, line in enumerate(lines):
                        if "PARAMETERS:" in line:
                            for sub_line in lines[index + 2 :]:  # noqa E203
                                if (
                                    "###################################################"  # noqa E501
                                    in sub_line
                                ):
                                    break
                                interval_match = re.search(
                                    interval_pattern, sub_line
                                )
                                freq_channel_match = re.search(
                                    freq_channel_pattern, sub_line
                                )
                                if interval_match:
                                    interval_value = int(
                                        interval_match.group(1)
                                    )
                                if freq_channel_match:
                                    freq_channel_value = int(
                                        freq_channel_match.group(1)
                                    )

                    if freq_channel_value:
                        start_frequency_channel = (
                            freq_channel_value * (400 / 512) * 1e6
                        )

                else:
                    # IF no channel or it equals 0
                    # try to fetch channel from folder name
                    folder_name = os.path.basename(
                        self.folder_path.rstrip("/")
                    )
                    match_channel = re.search(r"ch(\d+)", folder_name)

                    if match_channel:
                        # If a match is found, extract the channel number
                        start_frequency_channel = (
                            int(match_channel.group(1)) * (400 / 512) * 1e6
                        )

                    elif channel.isdigit() and len(channel) == 3:
                        start_frequency_channel = (
                            int(channel) * (400 / 512) * 1e6
                        )

                if start_frequency_channel:
                    if not em_min or start_frequency_channel < em_min:
                        em_min = float(start_frequency_channel)

                    if not em_max or start_frequency_channel > em_max:
                        em_max = float(start_frequency_channel)

                for dat_file in dat_files[targets][channel]:
                    # example var : channel_4_1604456020.385211.dat
                    # extract 1604456020.385211 timestamp

                    parts = dat_file.split("_")
                    if len(parts) == 3:
                        UNIXTIME = parts[2].split(".")[
                            0
                        ]  # Extract the part before the dot
                        UNIXTIME_MSEC = parts[2].split(".")[1]
                        timestamp = (
                            float(UNIXTIME) + float(UNIXTIME_MSEC) / 1000
                        )
                        if not min_time or timestamp < min_time:
                            min_time = timestamp

                    # Add file to context
                    target_metadata["files"].append(
                        self.add_file_to_metadata(
                            "", metadata_path_to_file + dat_file
                        )
                    )

            pb_identifier = random.randint(10000, 99999)
            while pb_identifier in self.used_numbers:
                pb_identifier = random.randint(10000, 99999)

            self.used_numbers.append(pb_identifier)

            target_metadata["config"][
                "processing_block"
            ] = "pb-aavs2-{}-{}".format(
                self.observation_date_time, pb_identifier
            )
            target_metadata["execution_block"] = "eb-m001-{}-{}".format(
                self.observation_date_time, self.eb_identifier
            )

            if not interval_value:
                interval_value = 900  # used as default interval

            # Converting times to MJD with 14 decimal cases
            target_metadata["obscore"]["t_min"] = float(
                round((min_time / 86400) + 40587, 14)
            )
            target_metadata["obscore"]["t_max"] = (
                target_metadata["obscore"]["t_min"] + interval_value
            )
            target_metadata["obscore"]["t_exptime"] = float(
                round(
                    target_metadata["obscore"]["t_max"]
                    - target_metadata["obscore"]["t_min"],
                    14,
                )
            )

            speed_of_light = 299792458  # m/s
            bandwidth = 6.25e6  # Assumption

            if em_min:
                target_metadata["obscore"]["em_min"] = float(
                    speed_of_light / em_min
                )
            else:
                target_metadata["obscore"]["em_min"] = "null"

            if em_min:
                target_metadata["obscore"]["em_max"] = float(
                    speed_of_light / em_max
                )
                target_metadata["obscore"]["em_res_power"] = float(
                    float(bandwidth) / em_min
                )
            else:
                target_metadata["obscore"]["em_max"] = "null"
                target_metadata["obscore"]["em_res_power"] = "null"

            target_metadata["files"].append(
                self.add_file_to_metadata(
                    "Metadata File Description according with ADR-55",
                    self.metadata_file_name,
                )
            )

            secondary_glob_paths = [
                self.folder_path + "",
                self.folder_path + "*/",
            ]

            secondary_files_list = self.get_secondary_files(
                secondary_glob_paths, self.extensions_for_secondary
            )

            for secondary_file in secondary_files_list:
                # Add file to context
                secondary_file_basename = str(secondary_file).replace(
                    self.folder_path, ""
                )
                target_metadata["files"].append(
                    self.add_file_to_metadata("", secondary_file_basename)
                )

            self.add_execution_block()

            metadata_folder_path = (
                self.metadata_path
                + self.folder_path
                + targets
                + "/"
                + "ska-data-product.yaml"
            )

            target_metadata["obscore"]["calib_level"] = 0

            print(metadata_folder_path)

            os.makedirs(os.path.dirname(metadata_folder_path), exist_ok=True)

            # Writing the dictionary to the YAML file
            with open(metadata_folder_path, "w", encoding="UTF-8") as file:
                yaml.dump(target_metadata, file)

    def group_dat_files(self):
        dat_files = []
        dat_targets = {}
        root_path_list = {}
        for ext in ("*.dat", "*.dat.gz"):
            dat_files.extend(
                glob.glob(
                    os.path.join(self.folder_path + "**/", ext), recursive=True
                )
            )

        for file in dat_files:
            file_path = str(file).replace(self.folder_path, "")

            parts = file_path.split("/")

            target_name = parts[0]

            target_path = str(file_path).replace(target_name, "").lstrip("/")

            target_parts = target_path.split("/")

            if target_name not in dat_targets:
                dat_targets[target_name] = {}

            if len(target_parts) == 1:
                if target_name not in root_path_list:
                    root_path_list[target_name] = []
                root_path_list[target_name].append(target_parts[0])
            else:
                dat_targets[target_name].update(
                    self.nested_dict(target_parts, dat_targets[target_name])
                )

        return dat_targets, root_path_list

    def nested_dict(self, lst, dat_targets):
        if len(lst) == 1:
            if isinstance(dat_targets, dict):
                dat_targets = []
            dat_targets.append(lst[0])
            return dat_targets
        else:
            if lst[0] not in dat_targets:
                dat_targets[lst[0]] = {}
            return {lst[0]: self.nested_dict(lst[1:], dat_targets[lst[0]])}


if __name__ == "__main__":
    # Create an argument parser
    parser = argparse.ArgumentParser(
        description="Process HDF5 files in a directory."
    )

    # Add an argument for the directory path
    parser.add_argument(
        "directory_path", help="Path to the directory containing HDF5 files"
    )

    # Add an argument for the eb_identifier
    parser.add_argument("eb_identifier", help="EB Identifier")

    # Add an argument for the eb_identifier
    parser.add_argument(
        "metadata_path", help="Path where metadata will be stored"
    )

    # Parse the command-line arguments
    args = parser.parse_args()

    # Access the directory path and eb_identifier from the arguments
    directory_path = args.directory_path
    eb_identifier = args.eb_identifier
    metadata_path = args.metadata_path

    # Now, you can use the 'directory_path'
    # and 'eb_identifier' variables in your code
    test = DATMetadata(directory_path, eb_identifier, metadata_path)

    test.process_targets()
