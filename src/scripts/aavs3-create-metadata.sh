#!/bin/bash
echo "*** aavs3-create-metadata ***"

filetype=hdf5
datafolderName=$1

if [[ -z "${datafolderName}" ]]; then
    echo "Data folder name is empty"
	exit 1
fi

# make sure it ends with a /
if [[ "${datafolderName}" != */ ]]; then
    datafolderName="${datafolderName}/"
fi

datapath=/app/data/$datafolderName
metadatapath=/app/metadata/

case $filetype in
	"hdf5")
		poetry run python3 /app/src/ska_low_aavs3_metadata_utility/hdf5_metadata_utility.py $datafolderName $datapath $metadatapath
		echo "hdf5 metadata-utility"
		;;
	"dada")
		#dada_metadata_utility.py $datapath $identifier $metadatapath
        echo "data metadata-utility not implemented"
		;;
	"dat")
        #dat_metadata_utility.py $datapath $identifier $metadatapath
        echo "dat metadata-utility not implemented"
 
		;;
	*)
		echo $filetype"  data file type not supported"
		;;
esac
