#!/bin/bash
echo "*** aavs3-migrate-metadata ***"

metadata_path=$1/ska-data-product.yaml
data_store_path=$2

pb=$(yq .config.processing_block $metadata_path) 
eb=$(yq .execution_block $metadata_path)
ss=$(yq .config.sub_system $metadata_path)

if [ -z "${pb:-}" ] || [ -z "$pb" ] || [ "$pb" == "null" ]; then
  echo "Variable pb is either null or empty"
  exit 1
fi

if [ -z "${eb:-}" ] || [ -z "$eb" ] || [ "$eb" == "null" ]; then
  echo "Variable eb is either null or empty"
  exit 1
fi

if [ -z "${ss:-}" ] || [ -z "$ss" ] || [ "$ss" == "null" ]; then
  echo "Variable ss is either null or empty"
  exit 1
fi

target_path=$data_store_path/$eb/$ss/$pb/

echo "moving "$metadata_path
echo "into "$target_path

rclone copy $metadata_path $target_path

if [ $? -eq 0 ]; then
  echo "Copy executed successfully"
  exit 0
else
  echo "Error: Copy failed with exit code $?"
  exit 1
fi